from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from citychat.models import *

from citychat.forms import *


@csrf_exempt
def mainpage(request):
	context_dir = {}
	comments = None
	replies = []
	user = None
	comment = None

	try:
		loggedin = request.user.is_authenticated
	except:
		pass
	if request.method == 'POST':
		try:
			comment = request.POST['comment']
		except:
			pass

	if not request.user.is_anonymous:
		user = request.user.userprofile

	if comment:
		if not loggedin:
			Comment.objects.create(comment=comment, commenter= None)
		else:
			Comment.objects.create(comment=comment, commenter= user)

	try:
		comments = Comment.objects.all().order_by("-datetime_created")
	except:
		pass
	try:
		for e in comments:
			try:
				replies_to_e = Reply.objects.filter(comment=e)
				replies.append(replies_to_e)
			except:
				pass
	except:
		pass

	context_dir.update({'loggedin':loggedin, 'comments': comments, 'replies' :replies, })
	# return HttpResponse(comments)
	return render_to_response("chatcentral.html", context_dir,  context_instance=RequestContext(request))


@csrf_exempt
def submit_response(request):
	user = None
	comment = None
	repl = None
	try:
		loggedin = request.user.is_authenticated
	except:
		pass


	if request.method == 'POST':
		# try:
		# 	comment = request.POST['comment']
		# except:
		# 	pass

		try:
			reply = request.POST['reply']
		except:
			pass
		try:
			replied_comment_id = request.POST['comment_id']
		except:
			pass

		# return HttpResponse(reply+replied_comment_id)

	# if comment:
	# 	if not loggedin:
	# 		Comment.objects.create(comment=comment, commenter= None)
	# 	else:
	# 		Comment.objects.create(comment=comment, commenter= user)

	# context_dir = {}
	# ajax = True


	# try:
	# 	comment = Comment.objects.get(id=replied_comment_id)
	# except:
	# 	pass
	# return HttpResponse(comments)
	# try:
	# 	replies_to_comment = Reply.objects.filter(comment=comment)
	# 	replies.append(replies_to_comment)
	# except:
	# 	pass

	# return HttpResponse(replies)

	# context_dir.update({'comment': comment, 'current_reply' :current_reply, 'ajax': ajax })
	# return HttpResponse(comments)
	
	# return render_to_response("threads.html", context_dir)

	if reply:
		current_reply = Reply.objects.create(reply=reply, replier= None, comment=Comment.objects.get(id=replied_comment_id))
		return HttpResponse(current_reply)


@csrf_exempt
def signup(request, path):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        password = request.POST['password']
        verify_password = request.POST['verify_password']
        if password == verify_password:
            user = User.objects.create_user(username=username, email=email, password=password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect('/'+ str(path))
    context_dir = {}
    return render_to_response('signup.html', context_dir)

@csrf_exempt
def login_handler(request, path):
    
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/'+ str(path))
            else:
                return HttpResponse("disabled account")
        else:
            return HttpResponse("Invalid Login")
    if request.user.is_authenticated():
        return redirect('/'+ str(path))
    return render_to_response('login.html', {'form': LoginForm() })


def logout_handler(request, path):
    logout(request)
    return redirect('/'+ str(path))
