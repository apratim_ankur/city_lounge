from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save

from datetime import date, timedelta

from django.core.files import File

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    datetime_created = models.DateTimeField(auto_now_add=True)


    def __unicode__(self):
	    return self.user.username

def user_post_save(sender, instance, created, **kwargs):
    """Create a user profile when a new user account is created"""
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(user_post_save, sender=User)

admin.site.register(UserProfile)



class Comment(models.Model):
	comment = models.CharField(max_length=10000, null=True, blank=True)
	commenter = models.ForeignKey(UserProfile,null=True, blank=True)
	datetime_created = models.DateTimeField(auto_now_add=True)
	
	def __unicode__(self):
	    return str(self.comment) + ' --by-- ' + str(self.commenter)
admin.site.register(Comment)


class Reply(models.Model):
	reply = models.CharField(max_length=10000, null=True, blank=True)
	replier = models.ForeignKey(UserProfile, null=True, blank=True)
	comment = models.ForeignKey(Comment)
	datetime_created = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return str(self.reply)  + ' --to topic-- ' + str(self.comment) + ' --by-- ' + str(self.replier)

admin.site.register(Reply)
