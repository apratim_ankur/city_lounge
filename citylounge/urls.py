from django.conf.urls import patterns, include, url
import citychat.views
from django.conf import settings


from django.contrib import admin
admin.autodiscover()
urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

	url(r'^$', citychat.views.mainpage),
	url(r'^submit_response/$', citychat.views.submit_response),

)
